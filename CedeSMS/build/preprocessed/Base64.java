/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cyberhacker
 */

 public class Base64 {

      private static final byte[] dTable = {
         -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,
         -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,
         -2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,62,-2,-2,-2,63,
         52,53,54,55,56,57,58,59,60,61,-2,-2,-2,-1,-2,-2,
         -2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
         15,16,17,18,19,20,21,22,23,24,25,-2,-2,-2,-2,-2,
         -2,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
         41,42,43,44,45,46,47,48,49,50,51,-2,-2,-2,-2,-2
     };

     private static final byte value(char c)
     {
         if(c > 127) return -2;
         return dTable[c];
     }

   private static        char[]  map1         = new char[64];
   static {
      int  i  = 0;
      for ( char c = 'A'; c <= 'Z'; c++ ) {
         map1[i++] = c;
      }
      for ( char c = 'a'; c <= 'z'; c++ ) {
         map1[i++] = c;
      }
      for ( char c = '0'; c <= '9'; c++ ) {
         map1[i++] = c;
      }
      map1[i++] = '+';
      map1[i++] = '/';
   }

   public static final byte[] base64Decode(String s)
     {
         int i, j = 0, l = 0;
         byte[] d, e = {0, 0, 0, 0};
         byte c; 
         for(i = 0; i < s.length(); i++){
             if((c = value(s.charAt(i))) > -2){
                 j++;
                 switch(j){
                 case 1:
                 case 2: if(c < 0) j = 0; break;
                 case 3: if(c < 0) {j = 0; l++;}; break;
                 case 4: if(c < 0) l += 2; else l += 3; j = 0;
                 }
             }
         }
         d = new byte[l]; j = 0; l = 0;
         for(i = 0; i < s.length(); i++){
             if((c = value(s.charAt(i))) > -2){
                 e[j] = c;
                 j++;
                 switch(j){
                 case 1:
                 case 2: if(c < 0) j = 0; break;
                 case 3: if(c < 0) {
                             j = 0;
                             d[l++] = (byte)((e[0] << 2) | ((e[1] >> 4) & 3));
                         }
                         break;
                 case 4: d[l++] = (byte)((e[0] << 2) | ((e[1] >> 4) & 3));
                         d[l++] = (byte)(((e[1] & 15) << 4) | (e[2] >> 2));
                         if(c > -1) d[l++] = (byte)(((e[2] & 3) << 6) | c);
                         j = 0;
                 }
             }
         }
         return d;
     }


   public static String base64Encode( byte[] in ) {
      int     iLen      = in.length;
      int     oDataLen  = ( iLen * 4 + 2 ) / 3;// output length without padding
      int     oLen      = ( ( iLen + 2 ) / 3 ) * 4;// output length including padding
      char[]  out       = new char[oLen];
      int     ip        = 0;
      int     op        = 0;
      int     i0;
      int     i1;
      int     i2;
      int     o0;
      int     o1;
      int     o2;
      int     o3;
      while ( ip < iLen ) {
         i0 = in[ip++] & 0xff;
         i1 = ip < iLen ? in[ip++] & 0xff : 0;
         i2 = ip < iLen ? in[ip++] & 0xff : 0;
         o0 = i0 >>> 2;
         o1 = ( ( i0 & 3 ) << 4 ) | ( i1 >>> 4 );
         o2 = ( ( i1 & 0xf ) << 2 ) | ( i2 >>> 6 );
         o3 = i2 & 0x3F;
         out[op++] = map1[o0];
         out[op++] = map1[o1];
         out[op] = op < oDataLen ? map1[o2] : '=';
         op++;
         out[op] = op < oDataLen ? map1[o3] : '=';
         op++;
      }
      return new String( out );
   }

}
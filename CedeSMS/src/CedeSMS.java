/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Command;
import javax.microedition.media.*;
import javax.microedition.media.control.*;
import javax.microedition.lcdui.CommandListener;
import java.util.*;
import java.io.*;
import javax.wireless.messaging.*;
import javax.microedition.io.*;
/**
 * @author Cyberhacker
 */
public class CedeSMS extends MIDlet implements CommandListener  {
    public Form mainForm;
    public ChoiceGroup myDropdown;
    public static Display display = null;
    private Command exit;
    private Command back;
    private Command newsms;
    private Command view;
    final Alert soundAlert = new Alert("CedeSMS");
    List rcvlist;

    // Compose SMS Screen
    public Form newSMSForm;
    public TextField recipientField;
    public TextField passwordField;
    public TextBox messageField;
    private Command send;
    private Command reallysend;
    private Command viewback;
    private Command decryptsms;
    private Command reallydecrypt;
    private Command cancelnew;
    private Command testcommand;
    private Command aboutcommand;

    Vector _smsList = new Vector();
    private MessageConnection _mc;
    private boolean _stop = false;
    private boolean _soundstop = false;
    private boolean _soundplaying = false;
    private static String _openString = "sms://:3590"; // See Connector implementation notes.
    private ListeningThread _listener;
    private SendThread _sender;
    private SoundThread _soundthread;
    private Random _rnd = new Random();
    private String strDemomessage = "This demonstration version of CedeSMS has expired. Please contact CedeSoft for further information.";

    public void startApp() {
        display = Display.getDisplay(this);
        mainForm = new Form("CedeSMS");

        _listener = new ListeningThread();
        _listener.start();

       // _sender = new SendThread();
        //_sender.start();

        // Add some sample elements to our vector array
        _smsList.addElement(new SmsMessage("01234123456","Sample Message"));
        _smsList.addElement(new SmsMessage("01235123457","Message Body 2"));
        //_smsList.addElement(new SmsMessage("01236123458","Message Body 3"));
        //_smsList.addElement(new SmsMessage("01237123459","Message Body 4"));
        //_smsList.addElement(new SmsMessage("01238123450","Message Body 5"));
        //_smsList.addElement(new SmsMessage("01239123451","Message Body 6"));
        //_smsList.addElement(new SmsMessage("01230123452","Message Body 7"));

        rcvlist = new List("CedeSMS - Messages", Choice.IMPLICIT);

        //rcvlist.append("01234123747", null);
        //rcvlist.append("01231125748", null);
        //rcvlist.append("01232135749", null);
        //rcvlist.append("01233147740", null);
        RefreshRcvList ();



        //myDropdown = new ChoiceGroup ("Menu", Choice.POPUP);
        //myDropdown.append("Hello", null);


        exit = new Command("Exit", Command.EXIT, 1);
        back = new Command("Back", Command.ITEM, 1);
        newsms = new Command("New Encrypted SMS", Command.ITEM, 1);
        testcommand = new Command ("Test", Command.ITEM, 1);
        view = new Command("Open SMS", Command.ITEM, 1);
        aboutcommand = new Command ("About", Command.ITEM, 1);

        mainForm.addCommand(exit);
        mainForm.addCommand(view);
        mainForm.addCommand(newsms);

        rcvlist.addCommand(exit);
        rcvlist.addCommand(view);
        rcvlist.addCommand(newsms);
        rcvlist.addCommand(aboutcommand);
        //rcvlist.addCommand(testcommand);


        rcvlist.setCommandListener(this);
        display.setCurrent(rcvlist);
    }

    public void RefreshRcvList ()
    {
        int a = 0;
        Font myFont = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL);
        //Font myFontBold = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_SMALL);
        //InputStream is = getClass().getResourceAsStream("Unread.png");


        //Image myImage = null;

        //try {
        //    myImage = Image.createImage(is);
        //}
        //catch (IOException ie)
        //{
        //}

        rcvlist.deleteAll();
        for (a=0;a<_smsList.size();a++) {
            SmsMessage smsmessage = (SmsMessage) _smsList.elementAt(a);            
            rcvlist.append("From: " + smsmessage.getAddress(), null);
            rcvlist.setFont (a, myFont);
        }

    }

    	public void commandAction(Command c, Displayable d) {
	// for exit from application
		if (c == exit) {
			destroyApp(true);
            notifyDestroyed();
		}

        if (c == view) {
            //showAlert ("View selected");
            //_smsList.addElement(new SmsMessage("01236123458","Message Body 3"));
            //RefreshRcvList();
            ViewSMSScreen();
        }

        if (c == newsms) {
            //showAlert ("New SMS");
            NewSMSScreen ();
        }

        if (c == send) {
            if (IsExpired () == false) {
                if (messageField.getString().length() > 3) {
                    SMSRecipientScreen();
                } else {
                    showAlert ("Message too short", 1);
                }
            } else {
                showAlert (strDemomessage, 1);
            }
            
        }

        if (c == List.SELECT_COMMAND) {
            ViewSMSScreen();
        }

        if (c == viewback) {
            display.setCurrent(rcvlist);
        }

        if (c == cancelnew) {
            display.setCurrent(rcvlist);
        }

        if (c == decryptsms) {
            if (IsExpired () == false) {
                DecryptSMSScreen();
            } else {
                showAlert (strDemomessage, 1);
            }
            
        }

        if (c == reallydecrypt) {
            DecryptSMS ();
        }

        if (c== reallysend) {
            SendSMS ();
        }

        if (c == testcommand) {
           //_smsList.addElement(new SmsMessage(CleanSMSAddress("07710978853"),"Hello"));
            //_messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
            //addReceivedMessage (address, msg);
            //RefreshRcvList();
            ToneTest();
        }

        if (c == aboutcommand) {
            showAlert ("CedeSMS v1.40.14.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.", 0);
        }
        // for returning on main screen

		}

    public void ToneTest ()
    {
        if (_soundplaying == false) {
            _soundplaying = true;
            _soundstop = false;
            _soundthread = new SoundThread ();
            _soundthread.start();
        }
    }

    public void NewSMSScreen ()
    {
        messageField = new TextBox("Message: ", "", 5000, TextField.ANY);
        send = new Command("Send", Command.ITEM, 1);
        cancelnew = new Command ("Cancel", Command.ITEM, 1);

        messageField.addCommand(send);
        messageField.addCommand(cancelnew);
        messageField.setCommandListener(this);
        
        //Base64.base64Encode(EncryptText("password123", "hello this is some plaintext").getBytes());
        //String strEncrypted = EncryptText ("password123", "hello this is some plaintext");
        //String strDecrypted = DecryptText("password123", strEncrypted);

        //messageField.setString(strEncrypted);
        

        display.setCurrent(messageField);
    }

    public void SMSRecipientScreen ()
    {
        newSMSForm = new Form("Send Encrypted SMS");
        recipientField = new TextField("Phone: ", "", 12, TextField.PHONENUMBER);
        passwordField = new TextField("Password: ", "", 20, TextField.PASSWORD);
        reallysend = new Command("Send", Command.ITEM, 1);
        cancelnew = new Command ("Cancel", Command.ITEM, 1);

        newSMSForm.append (recipientField);
        newSMSForm.append (passwordField);
        newSMSForm.addCommand(reallysend);
        newSMSForm.addCommand(cancelnew);

        newSMSForm.setCommandListener(this);
        display.setCurrent(newSMSForm);
    }

    public void ViewSMSScreen()
    {
        if (_soundplaying == true) {
            _soundthread.stop();
            _soundplaying = false;
        }
        
        messageField = new TextBox("Encrypted Message: ", "", 5000, TextField.ANY);
        
        //messageField.addCommand(send);
        

        //Base64.base64Encode(EncryptText("password123", "hello this is some plaintext").getBytes());
        //String strEncrypted = EncryptText ("password123", "hello this is some plaintext");
        //String strDecrypted = DecryptText("password123", strEncrypted);
        viewback = new Command("Back", Command.ITEM, 1);
        decryptsms = new Command ("Decrypt", Command.ITEM, 1);
        messageField.addCommand(viewback);
        messageField.addCommand(decryptsms);
        //messageField.setString(strEncrypted);
        messageField.setCommandListener(this);
        
        SmsMessage smsmessage = (SmsMessage) _smsList.elementAt(rcvlist.getSelectedIndex());
        messageField.setString(smsmessage.getMessage());


        display.setCurrent(messageField);
    }

    public void DecryptSMSScreen()
    {
        newSMSForm = new Form("Decrypt SMS");
        
        passwordField = new TextField("Password: ", "", 20, TextField.PASSWORD);
        reallydecrypt = new Command("Decrypt", Command.ITEM, 1);
        
        newSMSForm.append (passwordField);
        newSMSForm.addCommand(reallydecrypt);

        newSMSForm.setCommandListener(this);
        display.setCurrent(newSMSForm);
    }
    
    public void DecryptSMS ()
    {
        String strEncrypted = messageField.getString();

        String strFilterencrypted = strEncrypted.substring (3);
        String strHeader = strEncrypted.substring(0, 3);
        String strDecrypted = "";

        if (strHeader.compareTo ("#CS") == 0) {
            strDecrypted = DecryptText(passwordField.getString(), strFilterencrypted);
        }

        if (strHeader.compareTo ("#CT") == 0) {
            strDecrypted = DecryptTextEx(passwordField.getString(), strFilterencrypted);
        }

        messageField.setString(strDecrypted);
        
        display.setCurrent(messageField);
    }

    public boolean IsExpired ()
    {
           Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

           cal.set (Calendar.AM_PM, Calendar.PM);
           cal.set (Calendar.HOUR, 1);
           cal.set (Calendar.MINUTE, 0);
           cal.set (Calendar.SECOND, 0);
           cal.set (Calendar.DAY_OF_MONTH, 24);
           cal.set (Calendar.MONTH, Calendar.OCTOBER);
           cal.set (Calendar.YEAR, 2009);

           Date dtu = cal.getTime();

           Date dtCurrent = new Date ();

            if (dtCurrent.getTime () >= dtu.getTime()) {
                return true;
            } else {
                return false;
            }
    }

    public void SendSMS ()
    {
        String strMessage = messageField.getString();
        String strPassword = passwordField.getString();
        String strRecipient = recipientField.getString ();
        String strEncrypted = "";

        if (strPassword.length() > 4 ) {
            if (strMessage.length() > 3) {
                if (strRecipient.length() > 5) {
                    strEncrypted = "#CT" + EncryptTextEx (strPassword, strMessage);
                    if (sendex(strRecipient, strEncrypted) == true) {
                        
                        showAlert ("Encrypted SMS successfully sent.", 0);
                        display.setCurrent(rcvlist);
                    }
                    
                } else {
                    showAlert ("Phone number too short", 1);
                }
            } else {
                showAlert ("Message too short", 1);
            }
        } else {
            showAlert ("Password too short", 1);
        }
    }

    public boolean sendex (String strRecipient, String strMessage)
    {
        boolean success = true;
        try {
            //String addr = "sms://+447710978853";
            String addr = "sms://" + strRecipient + ":3590";
            MessageConnection conn = (MessageConnection) Connector.open(addr);
            TextMessage msg = (TextMessage)conn.newMessage(MessageConnection.TEXT_MESSAGE);
            //msg.setPayloadText("Hello World!");
            msg.setPayloadText(strMessage);
            conn.send(msg);
            success = true;
        } catch (Exception e) {
            success = false;
            showAlert ("Error Sending SMS", 1);
        }

        return success;
    }

    public void showAlert(String strMessage, int type) {
      if (type == 0) {
        soundAlert.setType(AlertType.INFO);  
      } else {
          if (type == 2) {
                soundAlert.setType(AlertType.CONFIRMATION);
          } else {
                soundAlert.setType(AlertType.ERROR);
          }
        
      }
        
      //soundAlert.setTimeout(20);
      soundAlert.setString(strMessage);
      display.setCurrent(soundAlert);
   }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public long Raise (long lValue, long lPower)
    {
        long lRes = lValue;
        int l=0;
        for (l=1;l<lPower;l++)
        {
            lRes = lRes * lValue;
        }
        return lRes;
    }

    public String Expand (String strInval)
    {
        //add(new RichTextField("Expand function called." ,Field.NON_FOCUSABLE));
        //String strInval = "237745";
        String cExp1 = "";
        String cExp2 = "";
        int c = 0;
        long lExp1;
        long lExp2;
        long lPower=0;
        String strResult = "";

        for (c=0;c<strInval.length ();c++)
        {
            if (c>0)
            {
                cExp1 = strInval.substring(c-1,c);
                cExp2 = strInval.substring(c,c+1);
                lExp1 = Long.parseLong(cExp1);
                lExp2 = Long.parseLong(cExp2);

                lPower = Raise(lExp1, lExp2);

                strResult += Long.toString(lPower);
                //add(new RichTextField(cExp1 ,Field.NON_FOCUSABLE));
                //add(new RichTextField(cExp2 ,Field.NON_FOCUSABLE));
            }
        }

        //add(new RichTextField("Expand Value: "+strResult ,Field.NON_FOCUSABLE));
        return strResult;
    }

    public String RepeatExpand (String strInval, int Length)
    {
        String strResult = "";
        String strNewResult = "";

        strResult = Expand(strInval);

        while (strResult.length() < Length)
        {
            strNewResult = Expand(strResult);
            strResult = strNewResult;
        }

        return strResult;
    }

    public String GenerateKey (String strPassword)
    {
        int a = 0;
        int curByte = 0;
        int bcurByte = 0;
        int lChecksum = 0;
        byte byteCurrent = 0;
        char charbyte;
        String strbyte = "";
        byte[] bPassword = strPassword.getBytes();
        String strNumerics = "";

        // Now we need to calculate a checksum on the numerics
        for (a=0;a<strPassword.length();a++)
        {
            curByte = bPassword[a];
            lChecksum+=curByte;
        }

        for (a=0;a<strPassword.length();a++)
        {
            curByte = bPassword[a];
            curByte+=lChecksum;

            bcurByte = curByte % 100;
            bPassword[a] = (byte) bcurByte;
        }

        for (a=0;a<strPassword.length();a++)
        {
            strNumerics+=String.valueOf(bPassword[a]);
        }

        strNumerics = RepeatExpand(strNumerics, 512);

        return strNumerics;
    }

    public String EncryptText (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword); // First generate the key needed from the password
        byte[] bPlaintext = strPlaintext.getBytes(); // Get the bytes of the string
        byte[] bEnctext = strPlaintext.getBytes(); // Get the bytes of the string for encoding
        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        String strBase64 = "";
        int curTrans = 0;

        for (b=0;b<strPlaintext.length();b++)
        {
            strCurtrans = strKey.substring(b, b+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];
            curByte+=curTrans;

            bEnctext[b] = curByte;
        }

        strEncrypted = new String(bEnctext);
        //strBase64 = ToBase64(bEnctext, strEncrypted.length());
        strBase64 = Base64.base64Encode(bEnctext);

        return strBase64;
        //return strEncrypted;
    }

    public int GetRand (int Max)
    {
        return _rnd.nextInt(Max);
    }

    public String EncryptTextEx (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword); // First generate the key needed from the password
        String strPlainex = strPlaintext + "0";

        byte[] bPlaintext = strPlaintext.getBytes(); // Get the bytes of the string
        byte[] bEnctext = strPlainex.getBytes(); // Get the bytes of the string for encoding
        byte curByte = 0;

        int rand = GetRand(47);
        byte brand = (byte) rand;

        String strCurtrans = "";
        String strEncrypted = "";
        String strBase64 = "";
        int curTrans = 0;

        for (b=0;b<strPlaintext.length();b++)
        {
            strCurtrans = strKey.substring(b+rand, b+rand+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];
            curByte+=curTrans;

            bEnctext[b+1] = curByte;
        }

        bEnctext[0] = brand;
        strEncrypted = new String(bEnctext);
        strBase64 = Base64.base64Encode(bEnctext);

        return strBase64;
    }

    public String DecryptText (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword); // First generate the key needed from the password
        //String strFrombase64 = FromBase64(strPlaintext);
        //byte[] bPlaintext = strFrombase64.getBytes(); // Get the bytes of the string
        //byte[] bPlaintext = FromBase64(strPlaintext);
        byte[] bPlaintext = Base64.base64Decode(strPlaintext);
        //byte[] bEnctext = strFrombase64.getBytes(); // Get the bytes of the string for encoding
        byte[] bEnctext = bPlaintext;
        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        int curTrans = 0;

        for (b=0;b<bPlaintext.length;b++)
        {
            strCurtrans = strKey.substring(b, b+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];
            curByte-=curTrans;

            bEnctext[b] = curByte;
        }

        strEncrypted = new String(bEnctext);

        return strEncrypted;
    }

    public String DecryptTextEx (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword); // First generate the key needed from the password
        //String strFrombase64 = FromBase64(strPlaintext);
        //byte[] bPlaintext = strFrombase64.getBytes(); // Get the bytes of the string
        byte[] bPlaintext = Base64.base64Decode(strPlaintext);
        //byte[] bEnctext = strFrombase64.getBytes(); // Get the bytes of the string for encoding
        byte[] bEnctext = bPlaintext;

        int rand = bPlaintext[0];

        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        int curTrans = 0;

        for (b=0;b<bPlaintext.length-1;b++)
        {
            strCurtrans = strKey.substring(b+rand, b+rand+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b+1];
            curByte-=curTrans;

            bEnctext[b] = curByte;
        }

        strEncrypted = new String(bEnctext);
        strEncrypted = strEncrypted.substring(0, strEncrypted.length()-1);

        return strEncrypted;
    }
    
    private void send(String addr, String data)
    {
        _sender.send(addr, data);
    }

        public String CleanSMSAddress (String address)
    {
        String strFirstpart = "";
        int iColonindex = 0;

        strFirstpart = address.substring(6);
        iColonindex = strFirstpart.indexOf(":");
        
        if (iColonindex > 0) {
            return strFirstpart.substring(0,iColonindex);
        } else {
            return strFirstpart;
        }

    }

       private void receivedSmsMessage(Message m)
    {
        String address = m.getAddress();
        String msg = null;
        
        if ( m instanceof TextMessage )
        {
            TextMessage tm = (TextMessage) m;
            msg = tm.getPayloadText();
        }
        
        //StringBuffer sb = new StringBuffer();
        //sb.append("Received:");
        //sb.append('\n');
        //sb.append("Destination:");
        //sb.append(address);
        //sb.append('\n');
        //sb.append("Data:");
        //sb.append(msg);
        //sb.append('\n');

        //updateStatus(sb.toString());
        
       _smsList.addElement(new SmsMessage(CleanSMSAddress(address),msg));
        //_messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
        //addReceivedMessage (address, msg);
       RefreshRcvList();
       ToneTest();
       showAlert ("Encrypted SMS Received.", 0);
    }

            // Inner Classes ------------------------------------------------------------
    private class ListeningThread extends Thread
    {
        private synchronized void stop()
        {
            _stop = true;

            try
            {
                if( _mc != null )
                {
                    // Close the connection so the thread will return.
                    _mc.close();
                }
            }
            catch (IOException e)
            {
                System.err.println(e.toString());
            }
        }

        public void run()
        {
            try
            {
                _mc = (MessageConnection)Connector.open(_openString); // Closed by the stop() method.

                for(;;)
                {
                    if ( _stop )
                    {
                        return;
                    }

                    Message m = _mc.receive();

                    receivedSmsMessage(m);
                }
            }
            catch (IOException e)
            {
                // Likely the stream was closed.
                System.err.println(e.toString());
            }
        }
    }

    private class SoundThread extends Thread
    {

        private synchronized void stop()
        {
           _soundstop = true;
        }

        public void run ()
        {
             for (;;) {
              try
              {

                if ( _soundstop )
                {
                    return;
                }

                InputStream is = getClass().getResourceAsStream("EncBeep1.mp3");
                Player p = Manager.createPlayer(is, "audio/mp3");
                p.start();

                try {
                    Thread.sleep (10000);
                }
                catch (InterruptedException ios) { }
                
                p.close();
              }
              catch (IOException ioe) { }
              catch (MediaException me) { }
             }
        }
    }

        private class SendThread extends Thread
    {
        private static final int TIMEOUT = 500; // ms


        // Create a vector of SmsMessage objects with an initial capacity of 5.
        // For this implementation it is unlikely that more than 5 msgs will be
        // queued at any one time.
        private Vector _msgs = new Vector(5);

        private volatile boolean _start = false;

        // Requests are queued.
        private synchronized void send(String address, String msg)
        {
            _start = true;
            _msgs.addElement(new SmsMessage(address, msg));
        }

        // Shutdown the thread.
        private synchronized void stop()
        {
            _stop = true;

            try
            {
                if ( _mc != null )
                {
                    _mc.close();
                }
            }
            catch (IOException e )
            {
                System.err.println(e);
                showAlert ("Error stopping SMS", 1);
            }
        }

        public void run()
        {

            for(;;)
            {
                // Thread control.
                while( !_start && !_stop)
                {
                    // Sleep for a bit so we don't spin.
                    try
                    {
                        sleep(TIMEOUT);
                    }
                    catch (InterruptedException e)
                    {
                        System.err.println(e.toString());
                    }
                }

                // Exit condition.
                if ( _stop )
                {
                    return;
                }

                while(true)
                {
                    //try
                    //{
                        SmsMessage sms = null;

                        synchronized (this)
                        {
                            if ( !_msgs.isEmpty() )
                            {
                                sms = (SmsMessage)_msgs.firstElement();

                                // Remove the element so we don't send it again.
                                _msgs.removeElement(sms);
                            }
                            else
                            {
                                _start = false;
                                break;
                            }
                        }

                        //_mc.send(sms.toMessage(_mc));

                    //}
                    //catch (IOException e)
                    //{
                        //System.err.println(e);
                        //updateStatus(e.toString());
                        //showAlert("Error Sending SMS");
                    //}
                }
            }
        }
    }
    

    private static final class SmsMessage
    {
        private String _address;
        private String _msg;

        private SmsMessage(String address, String msg)
        {
            _address = address;
            _msg = msg;
        }

        public String getAddress ()
        {
            return _address;
        }

        public String getMessage ()
        {
            return _msg;
        }

        private Message toMessage(MessageConnection mc)
        {
            TextMessage m = (TextMessage) mc.newMessage(MessageConnection.TEXT_MESSAGE , "//" + _address + ":3590");
            m.setPayloadText(_msg);



            return m;
        }
    }
}


    
